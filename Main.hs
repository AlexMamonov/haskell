import Homework3
import Prelude hiding (filter, map)

-- Homework4        

numPreds :: [(a -> Bool)] -> a -> Int
numPreds plist x = foldl (\ acc p -> if (p x) then acc+1 else acc) 0  plist

filterMapAnd :: [a -> Bool] -> a -> ([a -> Bool], [a -> Bool])
filterMapAnd flist a = (ft,ff)
                       where f = map (\ q -> (q,(q a))) flist
                             ft = map fst $ filter (\ z -> snd z) f
                             ff = map fst $ filter (\ z -> not (snd z)) f

--  head (fst (filterMapAnd [odd, even, (>5), (>4), (>1)] 6)) 7

-- findIndices :: (a -> Bool) -> [a] -> [Int]
-- findIndices p = foldr (\x acc n-> if p x then (n+1): acc else n+1 acc) []



-- Homework5

map            :: (a -> b) -> [a] -> [b]
map f []       = []
map f (x:xs)   = foldr (\y ys -> (f y):ys) [] xs

elem :: (Eq a) => a -> [a] -> Bool
elem x  =  foldl (\ s x -> if x == x then True else s) False

maximum :: (Ord a) => [a] -> a  
maximum = foldr1 (\x acc -> if x > acc then x else acc)  

filter :: (a -> Bool) -> [a] -> [a]  
filter p = foldr (\x acc -> if p x then x : acc else acc) []  

-- data Exp = Const Int
--       | Add Exp Exp
--       | Sub Exp Exp
--       | Mul Exp Exp


-- showExp :: Exp -> String
-- showExp x = case x of 
--       Const x -> show(x)
--       Add x y  -> "(" ++ showExp(x) ++ " + " ++ showExp(y) ++ ")"
--       Sub x y  -> "(" ++ showExp(x) ++ " - " ++ showExp(y) ++ ")"
--       Mul x y  -> "(" ++ showExp(x) ++ " * " ++ showExp(y) ++ ")"

-- eval :: Exp -> Int
-- eval x = case x of 
--       Const x -> x
--       Add x y  ->  eval(x) + eval(y)
--       Sub x y  ->  eval(x) - eval(y)
--       Mul x y  ->  eval(x) * eval(y)

-- applyDist :: Exp -> Exp 
-- applyDist Mul x (Sub y z)  ->  Sub (applyDist(Mul x y)) (applyDist(Mul x z))
-- applyDist Mul x (Add y z)  ->  Add (applyDist(Mul x y)) (applyDist(Mul x z))
-- applyDist Mul x y = Mul x y
-- applyDist e@(Const x) = e
data Bin = And | Or | If | Iff | Xor

instance Show Bin where
      show And = "&"
      show Or = "|"
      show If = "->"
      show Iff = "<->"
      show Xor = "+"

-- Precedence

prec And = 7
prec Or = 6
prec Xor = 6
prec If = 4
prec Iff = 4
prec Xor = 5

data Formula = 
      Var String |
      Const Bool | 
      Neg Formula | 
      BinOp Formula Bin Formula

showFormula ::Int -> Formula -> ShowS
showFormula _ (Var s) = showString s
showFormula _ (Const b) = shows (if b then 1 else 0)
showFormula _ (Neg f) = showChar '~' . showFormula 11 f 
showFormula pExt (BinOp f1 bin f2) = 
      let pInt = prec bin in
      showParen(pExt > pInt) $
      showFormula pInt f1 . 
      shows bin . 
      showFormula pInt f2

instance Show Formula where
      show f = showFormula 0 f ""

form1 = Neg ( BinOp (BinOp (Var "x") And (Var "y")) Or (Var "z"))

form2 = Neg ( BinOp (BinOp (Var "x") Or (Var "y")) And (Var "z"))



